package main;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FrenchMobNumber{
	
	public static void main(String[] args) {
		System.out.println("Veuillez saisir le num�ro");
		Scanner scanner = new Scanner(System.in);
		String number = scanner.nextLine();
		FrenchMobNumber fbm = new FrenchMobNumber();
		boolean french = fbm.isFrenchMobNumber(number);
		System.out.println (String.valueOf(french));
		

	}
	
	public boolean isFrenchMobNumber (String number) {
		//On d�finit le format d'un num�ro fran�ais
		String regex = "(?:\\+|00)33(6|7)\\d{8}";

		boolean french =number.matches(regex);
		
		return french;
	}
		
		
	
}
