package test;

import static org.junit.Assert.*;

//import java.util.regex.Pattern;

import org.junit.Test;
import main.FrenchMobNumber;

public class FrenchMobNumberTest  {

	@Test
	public void testIsFrenchMobNumber() {
		FrenchMobNumber fmn = new FrenchMobNumber();
		assertTrue(fmn.isFrenchMobNumber("0033600000000"));
		assertTrue(fmn.isFrenchMobNumber("+33600000000"));
		assertTrue(fmn.isFrenchMobNumber("0033700000000"));
		assertTrue(fmn.isFrenchMobNumber("+33700000000"));
	
		assertFalse("Echec, pas un num�ro de t�l�phone mobile fran�ais au format international", fmn.isFrenchMobNumber("azerty"));
		assertFalse("Echec, pas un num�ro de t�l�phone mobile fran�ais au format international", fmn.isFrenchMobNumber("33600000000"));
		assertFalse("Echec, pas un num�ro de t�l�phone mobile fran�ais au format international", fmn.isFrenchMobNumber("+33500000000"));
		assertFalse("Echec, pas un num�ro de t�l�phone mobile fran�ais au format international", fmn.isFrenchMobNumber("0033500000000"));
		assertFalse("Echec, pas un num�ro de t�l�phone mobile fran�ais au format international", fmn.isFrenchMobNumber("00000000"));
	}

}
